import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:staged_timer/injection.dart';
import 'package:staged_timer/presentation/core/app_widget.dart';

void main() {
  configureInjection(Environment.dev);

  runApp(const AppWidget());
}
