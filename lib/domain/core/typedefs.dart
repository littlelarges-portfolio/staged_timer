import 'package:dartz/dartz.dart';
import 'package:staged_timer/domain/core/errors/value_failures.dart';

typedef Value<T> = Either<ValueFailure<T>, T>;
