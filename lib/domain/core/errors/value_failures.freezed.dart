// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'value_failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ValueFailure<T> {
  StagedTimerValueFailure<T> get f => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(StagedTimerValueFailure<T> f) stagedTimer,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(StagedTimerValueFailure<T> f)? stagedTimer,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(StagedTimerValueFailure<T> f)? stagedTimer,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StagedTimer<T> value) stagedTimer,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StagedTimer<T> value)? stagedTimer,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StagedTimer<T> value)? stagedTimer,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValueFailureCopyWith<T, ValueFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValueFailureCopyWith<T, $Res> {
  factory $ValueFailureCopyWith(
          ValueFailure<T> value, $Res Function(ValueFailure<T>) then) =
      _$ValueFailureCopyWithImpl<T, $Res, ValueFailure<T>>;
  @useResult
  $Res call({StagedTimerValueFailure<T> f});

  $StagedTimerValueFailureCopyWith<T, $Res> get f;
}

/// @nodoc
class _$ValueFailureCopyWithImpl<T, $Res, $Val extends ValueFailure<T>>
    implements $ValueFailureCopyWith<T, $Res> {
  _$ValueFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? f = null,
  }) {
    return _then(_value.copyWith(
      f: null == f
          ? _value.f
          : f // ignore: cast_nullable_to_non_nullable
              as StagedTimerValueFailure<T>,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $StagedTimerValueFailureCopyWith<T, $Res> get f {
    return $StagedTimerValueFailureCopyWith<T, $Res>(_value.f, (value) {
      return _then(_value.copyWith(f: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$StagedTimerImplCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory _$$StagedTimerImplCopyWith(_$StagedTimerImpl<T> value,
          $Res Function(_$StagedTimerImpl<T>) then) =
      __$$StagedTimerImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({StagedTimerValueFailure<T> f});

  @override
  $StagedTimerValueFailureCopyWith<T, $Res> get f;
}

/// @nodoc
class __$$StagedTimerImplCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res, _$StagedTimerImpl<T>>
    implements _$$StagedTimerImplCopyWith<T, $Res> {
  __$$StagedTimerImplCopyWithImpl(
      _$StagedTimerImpl<T> _value, $Res Function(_$StagedTimerImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? f = null,
  }) {
    return _then(_$StagedTimerImpl<T>(
      null == f
          ? _value.f
          : f // ignore: cast_nullable_to_non_nullable
              as StagedTimerValueFailure<T>,
    ));
  }
}

/// @nodoc

class _$StagedTimerImpl<T> implements _StagedTimer<T> {
  const _$StagedTimerImpl(this.f);

  @override
  final StagedTimerValueFailure<T> f;

  @override
  String toString() {
    return 'ValueFailure<$T>.stagedTimer(f: $f)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StagedTimerImpl<T> &&
            (identical(other.f, f) || other.f == f));
  }

  @override
  int get hashCode => Object.hash(runtimeType, f);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$StagedTimerImplCopyWith<T, _$StagedTimerImpl<T>> get copyWith =>
      __$$StagedTimerImplCopyWithImpl<T, _$StagedTimerImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(StagedTimerValueFailure<T> f) stagedTimer,
  }) {
    return stagedTimer(f);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(StagedTimerValueFailure<T> f)? stagedTimer,
  }) {
    return stagedTimer?.call(f);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(StagedTimerValueFailure<T> f)? stagedTimer,
    required TResult orElse(),
  }) {
    if (stagedTimer != null) {
      return stagedTimer(f);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StagedTimer<T> value) stagedTimer,
  }) {
    return stagedTimer(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StagedTimer<T> value)? stagedTimer,
  }) {
    return stagedTimer?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StagedTimer<T> value)? stagedTimer,
    required TResult orElse(),
  }) {
    if (stagedTimer != null) {
      return stagedTimer(this);
    }
    return orElse();
  }
}

abstract class _StagedTimer<T> implements ValueFailure<T> {
  const factory _StagedTimer(final StagedTimerValueFailure<T> f) =
      _$StagedTimerImpl<T>;

  @override
  StagedTimerValueFailure<T> get f;
  @override
  @JsonKey(ignore: true)
  _$$StagedTimerImplCopyWith<T, _$StagedTimerImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$StagedTimerValueFailure<T> {
  T get failedValue => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) cantBeEmpty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? cantBeEmpty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? cantBeEmpty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CantBeEmpty<T> value) cantBeEmpty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CantBeEmpty<T> value)? cantBeEmpty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CantBeEmpty<T> value)? cantBeEmpty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $StagedTimerValueFailureCopyWith<T, StagedTimerValueFailure<T>>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StagedTimerValueFailureCopyWith<T, $Res> {
  factory $StagedTimerValueFailureCopyWith(StagedTimerValueFailure<T> value,
          $Res Function(StagedTimerValueFailure<T>) then) =
      _$StagedTimerValueFailureCopyWithImpl<T, $Res,
          StagedTimerValueFailure<T>>;
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class _$StagedTimerValueFailureCopyWithImpl<T, $Res,
        $Val extends StagedTimerValueFailure<T>>
    implements $StagedTimerValueFailureCopyWith<T, $Res> {
  _$StagedTimerValueFailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_value.copyWith(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CantBeEmptyImplCopyWith<T, $Res>
    implements $StagedTimerValueFailureCopyWith<T, $Res> {
  factory _$$CantBeEmptyImplCopyWith(_$CantBeEmptyImpl<T> value,
          $Res Function(_$CantBeEmptyImpl<T>) then) =
      __$$CantBeEmptyImplCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T failedValue});
}

/// @nodoc
class __$$CantBeEmptyImplCopyWithImpl<T, $Res>
    extends _$StagedTimerValueFailureCopyWithImpl<T, $Res, _$CantBeEmptyImpl<T>>
    implements _$$CantBeEmptyImplCopyWith<T, $Res> {
  __$$CantBeEmptyImplCopyWithImpl(
      _$CantBeEmptyImpl<T> _value, $Res Function(_$CantBeEmptyImpl<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failedValue = freezed,
  }) {
    return _then(_$CantBeEmptyImpl<T>(
      failedValue: freezed == failedValue
          ? _value.failedValue
          : failedValue // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$CantBeEmptyImpl<T> implements _CantBeEmpty<T> {
  const _$CantBeEmptyImpl({required this.failedValue});

  @override
  final T failedValue;

  @override
  String toString() {
    return 'StagedTimerValueFailure<$T>.cantBeEmpty(failedValue: $failedValue)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CantBeEmptyImpl<T> &&
            const DeepCollectionEquality()
                .equals(other.failedValue, failedValue));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failedValue));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CantBeEmptyImplCopyWith<T, _$CantBeEmptyImpl<T>> get copyWith =>
      __$$CantBeEmptyImplCopyWithImpl<T, _$CantBeEmptyImpl<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T failedValue) cantBeEmpty,
  }) {
    return cantBeEmpty(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T failedValue)? cantBeEmpty,
  }) {
    return cantBeEmpty?.call(failedValue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T failedValue)? cantBeEmpty,
    required TResult orElse(),
  }) {
    if (cantBeEmpty != null) {
      return cantBeEmpty(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CantBeEmpty<T> value) cantBeEmpty,
  }) {
    return cantBeEmpty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CantBeEmpty<T> value)? cantBeEmpty,
  }) {
    return cantBeEmpty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CantBeEmpty<T> value)? cantBeEmpty,
    required TResult orElse(),
  }) {
    if (cantBeEmpty != null) {
      return cantBeEmpty(this);
    }
    return orElse();
  }
}

abstract class _CantBeEmpty<T> implements StagedTimerValueFailure<T> {
  const factory _CantBeEmpty({required final T failedValue}) =
      _$CantBeEmptyImpl<T>;

  @override
  T get failedValue;
  @override
  @JsonKey(ignore: true)
  _$$CantBeEmptyImplCopyWith<T, _$CantBeEmptyImpl<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
