import 'package:freezed_annotation/freezed_annotation.dart';

part 'value_failures.freezed.dart';

@freezed
sealed class ValueFailure<T> with _$ValueFailure<T> {
  const factory ValueFailure.stagedTimer(StagedTimerValueFailure<T> f) =
      _StagedTimer<T>;
}

@freezed
sealed class StagedTimerValueFailure<T> with _$StagedTimerValueFailure<T> {
  const factory StagedTimerValueFailure.cantBeEmpty({required T failedValue}) =
      _CantBeEmpty<T>;
}
