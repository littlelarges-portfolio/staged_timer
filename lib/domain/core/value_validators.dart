import 'package:dartz/dartz.dart';
import 'package:staged_timer/domain/core/errors/value_failures.dart';
import 'package:staged_timer/domain/core/typedefs.dart';

Value<String> validateEmpty(
  String input,
) {
  if (input.isNotEmpty) {
    return right(input);
  } else {
    return left(
      ValueFailure.stagedTimer(
        StagedTimerValueFailure.cantBeEmpty(
          failedValue: input,
        ),
      ),
    );
  }
}
