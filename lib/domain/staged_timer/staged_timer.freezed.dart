// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'staged_timer.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$StagedTimer {
  Name get name => throw _privateConstructorUsedError;
  KtList<Stage> get stages => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $StagedTimerCopyWith<StagedTimer> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StagedTimerCopyWith<$Res> {
  factory $StagedTimerCopyWith(
          StagedTimer value, $Res Function(StagedTimer) then) =
      _$StagedTimerCopyWithImpl<$Res, StagedTimer>;
  @useResult
  $Res call({Name name, KtList<Stage> stages});
}

/// @nodoc
class _$StagedTimerCopyWithImpl<$Res, $Val extends StagedTimer>
    implements $StagedTimerCopyWith<$Res> {
  _$StagedTimerCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? stages = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      stages: null == stages
          ? _value.stages
          : stages // ignore: cast_nullable_to_non_nullable
              as KtList<Stage>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$StagedTimerImplCopyWith<$Res>
    implements $StagedTimerCopyWith<$Res> {
  factory _$$StagedTimerImplCopyWith(
          _$StagedTimerImpl value, $Res Function(_$StagedTimerImpl) then) =
      __$$StagedTimerImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Name name, KtList<Stage> stages});
}

/// @nodoc
class __$$StagedTimerImplCopyWithImpl<$Res>
    extends _$StagedTimerCopyWithImpl<$Res, _$StagedTimerImpl>
    implements _$$StagedTimerImplCopyWith<$Res> {
  __$$StagedTimerImplCopyWithImpl(
      _$StagedTimerImpl _value, $Res Function(_$StagedTimerImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? stages = null,
  }) {
    return _then(_$StagedTimerImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      stages: null == stages
          ? _value.stages
          : stages // ignore: cast_nullable_to_non_nullable
              as KtList<Stage>,
    ));
  }
}

/// @nodoc

class _$StagedTimerImpl implements _StagedTimer {
  const _$StagedTimerImpl({required this.name, required this.stages});

  @override
  final Name name;
  @override
  final KtList<Stage> stages;

  @override
  String toString() {
    return 'StagedTimer(name: $name, stages: $stages)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StagedTimerImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.stages, stages) || other.stages == stages));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name, stages);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$StagedTimerImplCopyWith<_$StagedTimerImpl> get copyWith =>
      __$$StagedTimerImplCopyWithImpl<_$StagedTimerImpl>(this, _$identity);
}

abstract class _StagedTimer implements StagedTimer {
  const factory _StagedTimer(
      {required final Name name,
      required final KtList<Stage> stages}) = _$StagedTimerImpl;

  @override
  Name get name;
  @override
  KtList<Stage> get stages;
  @override
  @JsonKey(ignore: true)
  _$$StagedTimerImplCopyWith<_$StagedTimerImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
