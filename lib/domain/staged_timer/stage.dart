import 'package:freezed_annotation/freezed_annotation.dart';

part 'stage.freezed.dart';

@freezed
abstract class Stage with _$Stage {
  const factory Stage({
    required Duration time,
  }) = _Stage;
}
