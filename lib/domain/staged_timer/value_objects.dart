import 'package:staged_timer/domain/core/value_objects.dart';
import 'package:staged_timer/domain/core/value_validators.dart';

class Name extends ValueObject<String> {
  factory Name(String input) => Name._(validateEmpty(input));

  const Name._(super.value);
}
