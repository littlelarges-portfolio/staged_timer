import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:staged_timer/domain/staged_timer/stage.dart';
import 'package:staged_timer/domain/staged_timer/value_objects.dart';

part 'staged_timer.freezed.dart';

@freezed
abstract class StagedTimer with _$StagedTimer {
  const factory StagedTimer({
    required Name name,
    required KtList<Stage> stages,
  }) = _StagedTimer;

  factory StagedTimer.empty() => StagedTimer(
        name: Name('Empty'),
        stages: const KtList.empty(),
      );
}
