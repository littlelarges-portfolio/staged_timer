import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:gap/gap.dart';
import 'package:kt_dart/kt.dart';
import 'package:staged_timer/application/staged_timer/add_staged_timer_form/add_staged_timer_form_bloc.dart';
import 'package:staged_timer/application/staged_timer/staged_timer_bloc.dart';
import 'package:staged_timer/domain/staged_timer/stage.dart';
import 'package:staged_timer/domain/staged_timer/staged_timer.dart';
import 'package:staged_timer/domain/staged_timer/value_objects.dart';
import 'package:staged_timer/presentation/core/colours.dart';
import 'package:staged_timer/presentation/core/common_widgets/st_app_bar_widget.dart';
import 'package:staged_timer/presentation/core/common_widgets/st_scaffold_widget.dart';
import 'package:staged_timer/presentation/core/gen/assets.gen.dart';
import 'package:staged_timer/presentation/core/helpers.dart';
import 'package:staged_timer/presentation/core/text_styles.dart';

class AddStagedTimerForm extends HookWidget {
  const AddStagedTimerForm({
    required this.addingStagedTimer,
    super.key,
  });

  final ValueNotifier<StagedTimer> addingStagedTimer;

  @override
  Widget build(BuildContext context) {
    final nameFormFieldController = useTextEditingController();
    final addingTime = useState(Duration.zero);
    return STScaffold(
      appBar: STAppBar(
        title: Text('Add', style: TextStyles.sfProText32SemiboldAliceBlue),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 25.r),
            child: TextButton(
              onPressed: () {
                context.read<AddStagedTimerFormBloc>().add(
                      AddStagedTimerFormEvent.donePressed(
                        stagedTimer: addingStagedTimer.value,
                      ),
                    );
              },
              child: Text(
                'Done',
                style: TextStyles.sfProText16SemiboldAliceBlue,
              ),
            ),
          ),
        ],
        centerTitle: false,
      ),
      body: BlocConsumer<AddStagedTimerFormBloc, AddStagedTimerFormState>(
        listenWhen: (previous, current) =>
            previous.validatedStagedTimer != current.validatedStagedTimer,
        listener: (context, state) {
          context.read<StagedTimerBloc>().add(
                StagedTimerEvent.added(stagedTimer: addingStagedTimer.value),
              );

          Navigator.pop(context);
        },
        builder: (context, state) {
          return Form(
            autovalidateMode: state.showErrorMessages,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.r),
                  child: TextFormField(
                    controller: nameFormFieldController,
                    style: TextStyles.sfProText20SemiboldSpaceCadet,
                    maxLength: 14,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colours.stateGray,
                      hintText: 'Name',
                      hintStyle:
                          TextStyles.sfProText20SemiboldAliceBlueWithOpacity05,
                      counterText: '',
                      border: UnderlineInputBorder(
                        borderRadius: BorderRadius.circular(12.r),
                      ),
                    ),
                    onChanged: (value) {
                      addingStagedTimer.value =
                          addingStagedTimer.value.copyWith(name: Name(value));

                      context.read<AddStagedTimerFormBloc>().add(
                            AddStagedTimerFormEvent.nameChanged(
                              nameStr: value,
                            ),
                          );
                    },
                    validator: (_) => context
                        .read<AddStagedTimerFormBloc>()
                        .state
                        .name
                        .value
                        .fold(
                          (f) => f.maybeWhen(
                            stagedTimer: (stagedTimerFailure) =>
                                stagedTimerFailure.maybeMap(
                              cantBeEmpty: (_) => "Can't be empty",
                              orElse: () => null,
                            ),
                            orElse: () => null,
                          ),
                          (r) => null,
                        ),
                  ),
                ),
                Gap(30.r),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.r),
                  child: Text(
                    'Stages',
                    style: TextStyles.sfProText32SemiboldAliceBlue,
                  ),
                ),
                Expanded(
                  child: ListView.separated(
                    shrinkWrap: true,
                    padding: EdgeInsets.symmetric(vertical: 30.r),
                    itemBuilder: (context, index) {
                      if (index < addingStagedTimer.value.stages.size) {
                        return Slidable(
                          endActionPane: ActionPane(
                            motion: const ScrollMotion(),
                            children: [
                              SlidableAction(
                                flex: 2,
                                onPressed: (context) {
                                  final stages = addingStagedTimer.value.stages
                                      .minusElement(
                                    addingStagedTimer.value.stages.get(index),
                                  );

                                  addingStagedTimer.value = addingStagedTimer
                                      .value
                                      .copyWith(stages: stages);
                                },
                                backgroundColor: Colours.red,
                                foregroundColor: Colors.white,
                                icon: Icons.delete,
                                label: 'Remove',
                              ),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              addingStagedTimer.value.stages[index].time
                                  .toFormatted(),
                              style: TextStyles.sfProText40SemiboldAliceBlue,
                            ),
                          ),
                        );
                      } else {
                        return IconButton(
                          onPressed: () {
                            _showTimePicker(
                              context: context,
                              addingStagedTimer: addingStagedTimer,
                              addingTime: addingTime,
                            );
                          },
                          icon: Assets.icons.add.svg(),
                        );
                      }
                    },
                    separatorBuilder: (context, index) => Gap(30.r),
                    itemCount: addingStagedTimer.value.stages.size + 1,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Future<Widget?> _showTimePicker({
    required BuildContext context,
    required ValueNotifier<StagedTimer> addingStagedTimer,
    required ValueNotifier<Duration> addingTime,
  }) {
    return showModalBottomSheet<Widget>(
      context: context,
      enableDrag: false,
      builder: (context) {
        return Wrap(
          children: [
            Container(
              padding: EdgeInsets.only(
                top: 10.r,
                bottom: 40.r,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20.r,
                    ),
                    child: TextButton(
                      onPressed: () {
                        final stages =
                            addingStagedTimer.value.stages.plusElement(
                          Stage(
                            time: addingTime.value,
                          ),
                        );

                        addingStagedTimer.value =
                            addingStagedTimer.value.copyWith(
                          stages: stages,
                        );

                        Navigator.pop(context);
                      },
                      child: Text(
                        'Done',
                        style: TextStyles.sfProText20RegularSpaceCadet,
                      ),
                    ),
                  ),
                  CupertinoTimerPicker(
                    mode: CupertinoTimerPickerMode.ms,
                    onTimerDurationChanged: (value) {
                      addingTime.value = value;
                    },
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
