import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:staged_timer/application/staged_timer/add_staged_timer_form/add_staged_timer_form_bloc.dart';
import 'package:staged_timer/domain/staged_timer/staged_timer.dart';
import 'package:staged_timer/injection.dart';
import 'package:staged_timer/presentation/staged_timer/add_staged_timer/widgets/add_staged_timer_form_widget.dart';

class AddStagedTimerPage extends HookWidget {
  const AddStagedTimerPage({super.key});

  static const route = '/add_staged_timer';

  @override
  Widget build(BuildContext context) {
    final addingStagedTimer = useState(StagedTimer.empty());
    return BlocProvider(
      create: (context) => getIt<AddStagedTimerFormBloc>(),
      child: AddStagedTimerForm(
        addingStagedTimer: addingStagedTimer,
      ),
    );
  }
}
