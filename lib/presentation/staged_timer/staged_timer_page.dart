import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:kt_dart/kt.dart';
import 'package:staged_timer/domain/staged_timer/staged_timer.dart';
import 'package:staged_timer/presentation/core/colours.dart';
import 'package:staged_timer/presentation/core/common_widgets/st_scaffold_widget.dart';
import 'package:staged_timer/presentation/core/gen/assets.gen.dart';
import 'package:staged_timer/presentation/core/helpers.dart';
import 'package:staged_timer/presentation/core/text_styles.dart';

class StagedTimerPage extends HookWidget {
  const StagedTimerPage({required this.stagedTimer, super.key});

  static const route = '/staged_timer';

  final StagedTimer stagedTimer;

  static const _tick = Duration(seconds: 1);

  @override
  Widget build(BuildContext context) {
    final timer = useState<KtList<Timer?>>(const KtList.empty());
    final durations = useState(
      KtList<Duration>.from(
        stagedTimer.stages.map((element) => element.time).asList(),
      ),
    );
    final currentTimer = useState(0);
    final stoped = useState(true);
    final paused = useState(false);

    final timerStatusColors = useState(
      KtList<Color>.from(
        stagedTimer.stages.map((element) => Colours.aliceBlue).asList(),
      ),
    );

    void changeTimerStatusColor({
      required int timerIndex,
      required Color color,
    }) {
      timerStatusColors.value = timerStatusColors.value.mapIndexed(
        (index, prevColor) => timerIndex == index ? color : prevColor,
      );
    }

    void stopTimer() {
      timer.value.forEach((element) => element?.cancel());

      durations.value = durations.value.mapIndexed(
        (index, duration) => stagedTimer.stages[index].time,
      );

      timerStatusColors.value =
          timerStatusColors.value.map((element) => Colours.aliceBlue);

      currentTimer.value = 0;

      stoped.value = true;
      paused.value = false;
    }

    void updateTimerDuration({required int timerIndex}) {
      durations.value = durations.value.mapIndexed(
        (index, duration) => index == timerIndex ? duration - _tick : duration,
      );
    }

    void startTimer({required int timerIndex}) {
      timer.value = KtList.from(
        List.generate(
          stagedTimer.stages.size,
          (index) {
            if (index == timerIndex) {
              changeTimerStatusColor(
                timerIndex: currentTimer.value,
                color: Colours.green,
              );

              return Timer.periodic(_tick, (timer) {
                if (durations.value[index] > _tick) {
                  updateTimerDuration(timerIndex: timerIndex);
                } else {
                  updateTimerDuration(timerIndex: timerIndex);

                  timer.cancel();

                  debugPrint('timer ${currentTimer.value} has finished');

                  changeTimerStatusColor(
                    timerIndex: currentTimer.value,
                    color: Colours.red,
                  );

                  if (currentTimer.value < stagedTimer.stages.size - 1) {
                    currentTimer.value++;

                    startTimer(timerIndex: currentTimer.value);
                  } else {
                    debugPrint('all timers have finished');

                    stopTimer();
                  }
                }
              });
            } else {
              return null;
            }
          },
        ),
      );

      stoped.value = false;
      paused.value = false;
    }

    void pauseTimer({required int timerIndex}) {
      timer.value[timerIndex]?.cancel();

      paused.value = true;
      stoped.value = false;
    }

    return STScaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 25.r,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  stagedTimer.name.getOrCrash(),
                  style: TextStyles.sfProText32SemiboldAliceBlue,
                ),
                IconButton(
                  onPressed: () {
                    // TODO(littlelarge): implement routing to the edit page
                  },
                  icon: Assets.icons.edit.svg(),
                ),
              ],
            ),
          ),
          Gap(30.r),
          Expanded(
            child: ListView.separated(
              padding: EdgeInsets.symmetric(vertical: 40.r),
              itemBuilder: (context, index) => Container(
                padding: EdgeInsets.symmetric(horizontal: 45.r),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      // timerDurations.value[index].toFormatted(),
                      durations.value[index].toFormatted(),
                      style: TextStyles.sfProText40SemiboldAliceBlue,
                    ),
                    Container(
                      height: 14.r,
                      width: 14.r,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: timerStatusColors.value[index],
                      ),
                    ),
                  ],
                ),
              ),
              separatorBuilder: (context, index) => Gap(30.r),
              itemCount: stagedTimer.stages.size,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (stoped.value && !paused.value)
                IconButton(
                  icon: Assets.icons.play.svg(
                    height: 60.r,
                    colorFilter:
                        ColorFilter.mode(Colours.green, BlendMode.srcIn),
                  ),
                  onPressed: () {
                    startTimer(timerIndex: currentTimer.value);
                  },
                )
              else if (!stoped.value && !paused.value)
                Row(
                  children: [
                    IconButton(
                      icon: Assets.icons.stop.svg(
                        height: 60.r,
                        colorFilter:
                            ColorFilter.mode(Colours.red, BlendMode.srcIn),
                      ),
                      onPressed: stopTimer,
                    ),
                    IconButton(
                      icon: Assets.icons.pause.svg(
                        height: 60.r,
                        colorFilter: ColorFilter.mode(
                          Colours.aliceBlue,
                          BlendMode.srcIn,
                        ),
                      ),
                      onPressed: () {
                        pauseTimer(timerIndex: currentTimer.value);
                      },
                    ),
                  ],
                )
              else
                Row(
                  children: [
                    IconButton(
                      icon: Assets.icons.stop.svg(
                        height: 60.r,
                        colorFilter:
                            ColorFilter.mode(Colours.red, BlendMode.srcIn),
                      ),
                      onPressed: stopTimer,
                    ),
                    IconButton(
                      icon: Assets.icons.continueTimer.svg(
                        height: 60.r,
                        colorFilter:
                            ColorFilter.mode(Colours.green, BlendMode.srcIn),
                      ),
                      onPressed: () {
                        startTimer(timerIndex: currentTimer.value);
                      },
                    ),
                  ],
                ),
            ],
          ),
        ],
      ),
    );
  }
}
