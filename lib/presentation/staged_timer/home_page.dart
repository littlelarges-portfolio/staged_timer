import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:staged_timer/application/staged_timer/staged_timer_bloc.dart';
import 'package:staged_timer/presentation/core/common_widgets/st_scaffold_widget.dart';
import 'package:staged_timer/presentation/core/gen/assets.gen.dart';
import 'package:staged_timer/presentation/core/text_styles.dart';
import 'package:staged_timer/presentation/routes/router.dart';
import 'package:staged_timer/presentation/staged_timer/widgets/staged_timer_card.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  static const route = '/home';

  @override
  Widget build(BuildContext context) {
    return STScaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 25.r),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Staged Timer',
                  style: TextStyles.sfProText32SemiboldAliceBlue,
                ),
                IconButton(
                  onPressed: () {
                    AddStagedTimerRoute().push<AddStagedTimerRoute>(context);
                  },
                  icon: Assets.icons.add.svg(),
                ),
              ],
            ),
          ),
          Gap(15.r),
          Expanded(
            child: BlocBuilder<StagedTimerBloc, StagedTimerState>(
              buildWhen: (previous, current) =>
                  previous.stagedTimers != current.stagedTimers,
              builder: (context, state) {
                return ListView.separated(
                  shrinkWrap: true,
                  padding: EdgeInsets.all(25.r),
                  itemBuilder: (context, index) => StagedTimerCard(
                    stagedTimer: state.stagedTimers[index],
                  ),
                  separatorBuilder: (context, index) => Gap(30.r),
                  itemCount: state.stagedTimers.size,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
