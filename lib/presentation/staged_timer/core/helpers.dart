import 'package:kt_dart/kt.dart';
import 'package:staged_timer/domain/staged_timer/staged_timer.dart';

extension StagedTimerX on StagedTimer {
  Duration get totalTime =>
      stages.fold(Duration.zero, (time, stage) => time + stage.time);
}
