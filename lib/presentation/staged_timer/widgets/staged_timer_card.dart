import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:staged_timer/application/staged_timer/staged_timer_bloc.dart';
import 'package:staged_timer/domain/staged_timer/staged_timer.dart';
import 'package:staged_timer/presentation/core/colours.dart';
import 'package:staged_timer/presentation/core/gen/assets.gen.dart';
import 'package:staged_timer/presentation/core/helpers.dart';
import 'package:staged_timer/presentation/core/text_styles.dart';
import 'package:staged_timer/presentation/routes/router.dart';
import 'package:staged_timer/presentation/staged_timer/core/helpers.dart';

class StagedTimerCard extends StatelessWidget {
  const StagedTimerCard({
    required this.stagedTimer,
    super.key,
  });

  final StagedTimer stagedTimer;

  BorderRadius get _borderRadius => BorderRadius.circular(15.r);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        borderRadius: _borderRadius,
        splashColor: Colours.black,
        overlayColor: WidgetStatePropertyAll(Colours.blackWithOpacity02),
        onTap: () {
          StagedTimerRoute($extra: stagedTimer).push<StagedTimerRoute>(context);
        },
        child: Ink(
          height: 100.r,
          decoration: BoxDecoration(
            color: Colours.frenchGray,
            borderRadius: _borderRadius,
          ),
          child: Row(
            children: [
              const Spacer(),
              const Spacer(),
              Text(
                stagedTimer.name.getOrCrash(),
                style: TextStyles.sfProText20SemiboldSpaceCadet,
              ),
              const Spacer(),
              Text(
                stagedTimer.totalTime.toFormatted(),
                style: TextStyles.sfProText32BoldSpaceCadet,
              ),
              const Spacer(),
              PopupMenuButton(
                color: Colours.spaceCadet,
                icon: Assets.icons.expand.svg(),
                itemBuilder: (context) {
                  return [
                    PopupMenuItem<dynamic>(
                      onTap: () {
                        context.read<StagedTimerBloc>().add(
                              StagedTimerEvent.removed(
                                stagedTimer: stagedTimer,
                              ),
                            );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Delete',
                            style: TextStyles.sfProText16SemiboldAliceBlue,
                          ),
                          Icon(Icons.delete, color: Colours.aliceBlue),
                        ],
                      ),
                    ),
                  ];
                },
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
