extension NumX on num {
  String toFormattedSeconds() => remainder(60).toString().padLeft(2, '0');
  String toFormattedMinutes() => remainder(60).toString().padLeft(2, '0');
}

extension DurationX on Duration {
  String toFormatted() =>
      '${inMinutes.toFormattedMinutes()}:${inSeconds.toFormattedSeconds()}';
}
