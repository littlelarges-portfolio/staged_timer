import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:staged_timer/presentation/core/colours.dart';
import 'package:staged_timer/presentation/core/gen/fonts.gen.dart';

extension NumExtension on num {
  double get fr => r * .85;
}

abstract class TextStyles {
  //* SF Pro Text

  static TextStyle get sfProText16SemiboldAliceBlue => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 16.fr,
        fontWeight: FontWeight.w600,
        color: Colours.aliceBlue,
      );

  static TextStyle get sfProText32SemiboldAliceBlue => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 32.fr,
        fontWeight: FontWeight.w600,
        color: Colours.aliceBlue,
      );

  static TextStyle get sfProText32BoldSpaceCadet => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 32.fr,
        fontWeight: FontWeight.bold,
        color: Colours.spaceCadet,
        letterSpacing: -2.r,
      );

  static TextStyle get sfProText40SemiboldAliceBlue => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 40.fr,
        fontWeight: FontWeight.w600,
        color: Colours.aliceBlue,
      );

  static TextStyle get sfProText20RegularSpaceCadet => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 20.fr,
        fontWeight: FontWeight.normal,
        color: Colours.spaceCadet,
      );

  static TextStyle get sfProText20SemiboldSpaceCadet => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 20.fr,
        fontWeight: FontWeight.w600,
        color: Colours.spaceCadet,
      );
  static TextStyle get sfProText20SemiboldAliceBlueWithOpacity05 => TextStyle(
        fontFamily: FontFamily.sFProText,
        fontSize: 20.fr,
        fontWeight: FontWeight.w600,
        color: Colours.aliceBlue.withOpacity(.5),
      );
}
