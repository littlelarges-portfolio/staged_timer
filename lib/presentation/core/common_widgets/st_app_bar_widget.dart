import 'package:flutter/material.dart';
import 'package:staged_timer/presentation/core/colours.dart';

class STAppBar extends StatelessWidget implements PreferredSizeWidget {
  const STAppBar({
    super.key,
    this.title,
    this.actions,
    this.centerTitle,
  });

  final Widget? title;
  final List<Widget>? actions;
  final bool? centerTitle;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colours.spaceCadet,
      foregroundColor: Colours.aliceBlue,
      title: title,
      actions: actions,
      centerTitle: centerTitle,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(56);
}
