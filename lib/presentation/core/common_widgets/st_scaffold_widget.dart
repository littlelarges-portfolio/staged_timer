import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:staged_timer/presentation/core/colours.dart';

class STScaffold extends StatelessWidget {
  const STScaffold({
    super.key,
    this.body,
    this.appBar,
  });

  final Widget? body;
  final PreferredSizeWidget? appBar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colours.spaceCadet,
      appBar: appBar,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(
            top: 25.r,
          ),
          child: body,
        ),
      ),
    );
  }
}
