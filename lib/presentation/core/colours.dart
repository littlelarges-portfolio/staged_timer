import 'dart:ui';

abstract class Colours {
  static Color get white => const Color(0xFFFFFFFF);
  static Color get black => const Color(0xFF000000);
  static Color get blackWithOpacity02 =>
      const Color(0xFF000000).withOpacity(.2);
  static Color get red => const Color(0xFFDE7070);
  static Color get green => const Color(0xFF8DD799);
  static Color get spaceCadet => const Color(0xFF2A324B);
  static Color get stateGray => const Color(0xFF767B91);
  static Color get frenchGray => const Color(0xFFC7CCDB);
  static Color get aliceBlue => const Color(0xFFE1E5EE);
}
