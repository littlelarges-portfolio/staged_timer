import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:staged_timer/application/staged_timer/staged_timer_bloc.dart';
import 'package:staged_timer/injection.dart';
import 'package:staged_timer/presentation/routes/router.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (_, child) {
        return BlocProvider(
          create: (context) => getIt<StagedTimerBloc>(),
          child: MaterialApp.router(
            debugShowCheckedModeBanner: false,
            title: 'Staged Timer',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              primarySwatch: Colors.deepPurple,
              textTheme: Typography.englishLike2018.apply(fontSizeFactor: 1.sp),
            ),
            routerConfig: router,
          ),
        );
      },
    );
  }
}
