// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'router.dart';

// **************************************************************************
// GoRouterGenerator
// **************************************************************************

List<RouteBase> get $appRoutes => [
      $stagedTimerRoute,
      $homeRoute,
      $addStagedTimerRoute,
    ];

RouteBase get $stagedTimerRoute => GoRouteData.$route(
      path: '/staged_timer',
      factory: $StagedTimerRouteExtension._fromState,
    );

extension $StagedTimerRouteExtension on StagedTimerRoute {
  static StagedTimerRoute _fromState(GoRouterState state) => StagedTimerRoute(
        $extra: state.extra as StagedTimer,
      );

  String get location => GoRouteData.$location(
        '/staged_timer',
      );

  void go(BuildContext context) => context.go(location, extra: $extra);

  Future<T?> push<T>(BuildContext context) =>
      context.push<T>(location, extra: $extra);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location, extra: $extra);

  void replace(BuildContext context) =>
      context.replace(location, extra: $extra);
}

RouteBase get $homeRoute => GoRouteData.$route(
      path: '/home',
      factory: $HomeRouteExtension._fromState,
    );

extension $HomeRouteExtension on HomeRoute {
  static HomeRoute _fromState(GoRouterState state) => HomeRoute();

  String get location => GoRouteData.$location(
        '/home',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

RouteBase get $addStagedTimerRoute => GoRouteData.$route(
      path: '/add_staged_timer',
      factory: $AddStagedTimerRouteExtension._fromState,
    );

extension $AddStagedTimerRouteExtension on AddStagedTimerRoute {
  static AddStagedTimerRoute _fromState(GoRouterState state) =>
      AddStagedTimerRoute();

  String get location => GoRouteData.$location(
        '/add_staged_timer',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}
