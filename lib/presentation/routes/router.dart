import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:staged_timer/domain/staged_timer/staged_timer.dart';
import 'package:staged_timer/presentation/staged_timer/add_staged_timer/add_staged_timer_page.dart';
import 'package:staged_timer/presentation/staged_timer/home_page.dart';
import 'package:staged_timer/presentation/staged_timer/staged_timer_page.dart';

part 'router.g.dart';

final router =
    GoRouter(routes: $appRoutes, initialLocation: HomePage.route);

@TypedGoRoute<StagedTimerRoute>(
  path: StagedTimerPage.route,
)
class StagedTimerRoute extends GoRouteData {
  StagedTimerRoute({required this.$extra});

  final StagedTimer $extra;

  @override
  Widget build(BuildContext context, GoRouterState state) => StagedTimerPage(
        stagedTimer: $extra,
      );
}

@TypedGoRoute<HomeRoute>(
  path: HomePage.route,
)
class HomeRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) => const HomePage();
}

@TypedGoRoute<AddStagedTimerRoute>(
  path: AddStagedTimerPage.route,
)
class AddStagedTimerRoute extends GoRouteData {
  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const AddStagedTimerPage();
}
