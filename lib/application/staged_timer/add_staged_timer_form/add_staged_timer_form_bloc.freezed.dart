// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'add_staged_timer_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AddStagedTimerFormEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(StagedTimer stagedTimer) donePressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(StagedTimer stagedTimer)? donePressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(StagedTimer stagedTimer)? donePressed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_DonePressed value) donePressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_DonePressed value)? donePressed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_DonePressed value)? donePressed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddStagedTimerFormEventCopyWith<$Res> {
  factory $AddStagedTimerFormEventCopyWith(AddStagedTimerFormEvent value,
          $Res Function(AddStagedTimerFormEvent) then) =
      _$AddStagedTimerFormEventCopyWithImpl<$Res, AddStagedTimerFormEvent>;
}

/// @nodoc
class _$AddStagedTimerFormEventCopyWithImpl<$Res,
        $Val extends AddStagedTimerFormEvent>
    implements $AddStagedTimerFormEventCopyWith<$Res> {
  _$AddStagedTimerFormEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$NameChangedImplCopyWith<$Res> {
  factory _$$NameChangedImplCopyWith(
          _$NameChangedImpl value, $Res Function(_$NameChangedImpl) then) =
      __$$NameChangedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String nameStr});
}

/// @nodoc
class __$$NameChangedImplCopyWithImpl<$Res>
    extends _$AddStagedTimerFormEventCopyWithImpl<$Res, _$NameChangedImpl>
    implements _$$NameChangedImplCopyWith<$Res> {
  __$$NameChangedImplCopyWithImpl(
      _$NameChangedImpl _value, $Res Function(_$NameChangedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? nameStr = null,
  }) {
    return _then(_$NameChangedImpl(
      nameStr: null == nameStr
          ? _value.nameStr
          : nameStr // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$NameChangedImpl implements _NameChanged {
  const _$NameChangedImpl({required this.nameStr});

  @override
  final String nameStr;

  @override
  String toString() {
    return 'AddStagedTimerFormEvent.nameChanged(nameStr: $nameStr)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NameChangedImpl &&
            (identical(other.nameStr, nameStr) || other.nameStr == nameStr));
  }

  @override
  int get hashCode => Object.hash(runtimeType, nameStr);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NameChangedImplCopyWith<_$NameChangedImpl> get copyWith =>
      __$$NameChangedImplCopyWithImpl<_$NameChangedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(StagedTimer stagedTimer) donePressed,
  }) {
    return nameChanged(nameStr);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(StagedTimer stagedTimer)? donePressed,
  }) {
    return nameChanged?.call(nameStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(StagedTimer stagedTimer)? donePressed,
    required TResult orElse(),
  }) {
    if (nameChanged != null) {
      return nameChanged(nameStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_DonePressed value) donePressed,
  }) {
    return nameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_DonePressed value)? donePressed,
  }) {
    return nameChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_DonePressed value)? donePressed,
    required TResult orElse(),
  }) {
    if (nameChanged != null) {
      return nameChanged(this);
    }
    return orElse();
  }
}

abstract class _NameChanged implements AddStagedTimerFormEvent {
  const factory _NameChanged({required final String nameStr}) =
      _$NameChangedImpl;

  String get nameStr;
  @JsonKey(ignore: true)
  _$$NameChangedImplCopyWith<_$NameChangedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DonePressedImplCopyWith<$Res> {
  factory _$$DonePressedImplCopyWith(
          _$DonePressedImpl value, $Res Function(_$DonePressedImpl) then) =
      __$$DonePressedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({StagedTimer stagedTimer});

  $StagedTimerCopyWith<$Res> get stagedTimer;
}

/// @nodoc
class __$$DonePressedImplCopyWithImpl<$Res>
    extends _$AddStagedTimerFormEventCopyWithImpl<$Res, _$DonePressedImpl>
    implements _$$DonePressedImplCopyWith<$Res> {
  __$$DonePressedImplCopyWithImpl(
      _$DonePressedImpl _value, $Res Function(_$DonePressedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stagedTimer = null,
  }) {
    return _then(_$DonePressedImpl(
      stagedTimer: null == stagedTimer
          ? _value.stagedTimer
          : stagedTimer // ignore: cast_nullable_to_non_nullable
              as StagedTimer,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $StagedTimerCopyWith<$Res> get stagedTimer {
    return $StagedTimerCopyWith<$Res>(_value.stagedTimer, (value) {
      return _then(_value.copyWith(stagedTimer: value));
    });
  }
}

/// @nodoc

class _$DonePressedImpl implements _DonePressed {
  const _$DonePressedImpl({required this.stagedTimer});

  @override
  final StagedTimer stagedTimer;

  @override
  String toString() {
    return 'AddStagedTimerFormEvent.donePressed(stagedTimer: $stagedTimer)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DonePressedImpl &&
            (identical(other.stagedTimer, stagedTimer) ||
                other.stagedTimer == stagedTimer));
  }

  @override
  int get hashCode => Object.hash(runtimeType, stagedTimer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DonePressedImplCopyWith<_$DonePressedImpl> get copyWith =>
      __$$DonePressedImplCopyWithImpl<_$DonePressedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String nameStr) nameChanged,
    required TResult Function(StagedTimer stagedTimer) donePressed,
  }) {
    return donePressed(stagedTimer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String nameStr)? nameChanged,
    TResult? Function(StagedTimer stagedTimer)? donePressed,
  }) {
    return donePressed?.call(stagedTimer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String nameStr)? nameChanged,
    TResult Function(StagedTimer stagedTimer)? donePressed,
    required TResult orElse(),
  }) {
    if (donePressed != null) {
      return donePressed(stagedTimer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_NameChanged value) nameChanged,
    required TResult Function(_DonePressed value) donePressed,
  }) {
    return donePressed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_NameChanged value)? nameChanged,
    TResult? Function(_DonePressed value)? donePressed,
  }) {
    return donePressed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_NameChanged value)? nameChanged,
    TResult Function(_DonePressed value)? donePressed,
    required TResult orElse(),
  }) {
    if (donePressed != null) {
      return donePressed(this);
    }
    return orElse();
  }
}

abstract class _DonePressed implements AddStagedTimerFormEvent {
  const factory _DonePressed({required final StagedTimer stagedTimer}) =
      _$DonePressedImpl;

  StagedTimer get stagedTimer;
  @JsonKey(ignore: true)
  _$$DonePressedImplCopyWith<_$DonePressedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AddStagedTimerFormState {
  Name get name => throw _privateConstructorUsedError;
  AutovalidateMode get showErrorMessages => throw _privateConstructorUsedError;
  StagedTimer? get validatedStagedTimer => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AddStagedTimerFormStateCopyWith<AddStagedTimerFormState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddStagedTimerFormStateCopyWith<$Res> {
  factory $AddStagedTimerFormStateCopyWith(AddStagedTimerFormState value,
          $Res Function(AddStagedTimerFormState) then) =
      _$AddStagedTimerFormStateCopyWithImpl<$Res, AddStagedTimerFormState>;
  @useResult
  $Res call(
      {Name name,
      AutovalidateMode showErrorMessages,
      StagedTimer? validatedStagedTimer});

  $StagedTimerCopyWith<$Res>? get validatedStagedTimer;
}

/// @nodoc
class _$AddStagedTimerFormStateCopyWithImpl<$Res,
        $Val extends AddStagedTimerFormState>
    implements $AddStagedTimerFormStateCopyWith<$Res> {
  _$AddStagedTimerFormStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? showErrorMessages = null,
    Object? validatedStagedTimer = freezed,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      showErrorMessages: null == showErrorMessages
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as AutovalidateMode,
      validatedStagedTimer: freezed == validatedStagedTimer
          ? _value.validatedStagedTimer
          : validatedStagedTimer // ignore: cast_nullable_to_non_nullable
              as StagedTimer?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $StagedTimerCopyWith<$Res>? get validatedStagedTimer {
    if (_value.validatedStagedTimer == null) {
      return null;
    }

    return $StagedTimerCopyWith<$Res>(_value.validatedStagedTimer!, (value) {
      return _then(_value.copyWith(validatedStagedTimer: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$AddStagedTimerFormStateImplCopyWith<$Res>
    implements $AddStagedTimerFormStateCopyWith<$Res> {
  factory _$$AddStagedTimerFormStateImplCopyWith(
          _$AddStagedTimerFormStateImpl value,
          $Res Function(_$AddStagedTimerFormStateImpl) then) =
      __$$AddStagedTimerFormStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Name name,
      AutovalidateMode showErrorMessages,
      StagedTimer? validatedStagedTimer});

  @override
  $StagedTimerCopyWith<$Res>? get validatedStagedTimer;
}

/// @nodoc
class __$$AddStagedTimerFormStateImplCopyWithImpl<$Res>
    extends _$AddStagedTimerFormStateCopyWithImpl<$Res,
        _$AddStagedTimerFormStateImpl>
    implements _$$AddStagedTimerFormStateImplCopyWith<$Res> {
  __$$AddStagedTimerFormStateImplCopyWithImpl(
      _$AddStagedTimerFormStateImpl _value,
      $Res Function(_$AddStagedTimerFormStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? showErrorMessages = null,
    Object? validatedStagedTimer = freezed,
  }) {
    return _then(_$AddStagedTimerFormStateImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      showErrorMessages: null == showErrorMessages
          ? _value.showErrorMessages
          : showErrorMessages // ignore: cast_nullable_to_non_nullable
              as AutovalidateMode,
      validatedStagedTimer: freezed == validatedStagedTimer
          ? _value.validatedStagedTimer
          : validatedStagedTimer // ignore: cast_nullable_to_non_nullable
              as StagedTimer?,
    ));
  }
}

/// @nodoc

class _$AddStagedTimerFormStateImpl implements _AddStagedTimerFormState {
  const _$AddStagedTimerFormStateImpl(
      {required this.name,
      required this.showErrorMessages,
      this.validatedStagedTimer});

  @override
  final Name name;
  @override
  final AutovalidateMode showErrorMessages;
  @override
  final StagedTimer? validatedStagedTimer;

  @override
  String toString() {
    return 'AddStagedTimerFormState(name: $name, showErrorMessages: $showErrorMessages, validatedStagedTimer: $validatedStagedTimer)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddStagedTimerFormStateImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.showErrorMessages, showErrorMessages) ||
                other.showErrorMessages == showErrorMessages) &&
            (identical(other.validatedStagedTimer, validatedStagedTimer) ||
                other.validatedStagedTimer == validatedStagedTimer));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, name, showErrorMessages, validatedStagedTimer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AddStagedTimerFormStateImplCopyWith<_$AddStagedTimerFormStateImpl>
      get copyWith => __$$AddStagedTimerFormStateImplCopyWithImpl<
          _$AddStagedTimerFormStateImpl>(this, _$identity);
}

abstract class _AddStagedTimerFormState implements AddStagedTimerFormState {
  const factory _AddStagedTimerFormState(
      {required final Name name,
      required final AutovalidateMode showErrorMessages,
      final StagedTimer? validatedStagedTimer}) = _$AddStagedTimerFormStateImpl;

  @override
  Name get name;
  @override
  AutovalidateMode get showErrorMessages;
  @override
  StagedTimer? get validatedStagedTimer;
  @override
  @JsonKey(ignore: true)
  _$$AddStagedTimerFormStateImplCopyWith<_$AddStagedTimerFormStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
