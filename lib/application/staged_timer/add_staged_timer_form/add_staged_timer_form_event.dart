part of 'add_staged_timer_form_bloc.dart';

@freezed
class AddStagedTimerFormEvent with _$AddStagedTimerFormEvent {
  const factory AddStagedTimerFormEvent.nameChanged({
    required String nameStr,
  }) = _NameChanged;
  const factory AddStagedTimerFormEvent.donePressed({
    required StagedTimer stagedTimer,
  }) = _DonePressed;
}
