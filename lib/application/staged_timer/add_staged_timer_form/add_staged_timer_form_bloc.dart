import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:staged_timer/domain/staged_timer/staged_timer.dart';
import 'package:staged_timer/domain/staged_timer/value_objects.dart';

part 'add_staged_timer_form_event.dart';
part 'add_staged_timer_form_state.dart';
part 'add_staged_timer_form_bloc.freezed.dart';

@injectable
class AddStagedTimerFormBloc
    extends Bloc<AddStagedTimerFormEvent, AddStagedTimerFormState> {
  AddStagedTimerFormBloc() : super(AddStagedTimerFormState.initial()) {
    on<AddStagedTimerFormEvent>((event, emit) {
      event.map(
        nameChanged: (e) {
          emit(state.copyWith(name: Name(e.nameStr)));
        },
        donePressed: (e) {
          final isNameValid = state.name.isValid();

          if (isNameValid) {
            emit(
              state.copyWith(validatedStagedTimer: e.stagedTimer),
            );
          }

          emit(
            state.copyWith(
              showErrorMessages: AutovalidateMode.always,
            ),
          );
        },
      );
    });
  }
}
