part of 'add_staged_timer_form_bloc.dart';

@freezed
class AddStagedTimerFormState with _$AddStagedTimerFormState {
  const factory AddStagedTimerFormState({
    required Name name,
    required AutovalidateMode showErrorMessages,
    StagedTimer? validatedStagedTimer,
  }) = _AddStagedTimerFormState;

  factory AddStagedTimerFormState.initial() => AddStagedTimerFormState(
        name: Name(''),
        showErrorMessages: AutovalidateMode.disabled,
      );
}
