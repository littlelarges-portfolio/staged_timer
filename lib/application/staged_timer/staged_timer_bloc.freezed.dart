// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'staged_timer_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$StagedTimerEvent {
  StagedTimer get stagedTimer => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(StagedTimer stagedTimer) added,
    required TResult Function(StagedTimer stagedTimer) removed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(StagedTimer stagedTimer)? added,
    TResult? Function(StagedTimer stagedTimer)? removed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(StagedTimer stagedTimer)? added,
    TResult Function(StagedTimer stagedTimer)? removed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) added,
    required TResult Function(_Removed value) removed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? added,
    TResult? Function(_Removed value)? removed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? added,
    TResult Function(_Removed value)? removed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $StagedTimerEventCopyWith<StagedTimerEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StagedTimerEventCopyWith<$Res> {
  factory $StagedTimerEventCopyWith(
          StagedTimerEvent value, $Res Function(StagedTimerEvent) then) =
      _$StagedTimerEventCopyWithImpl<$Res, StagedTimerEvent>;
  @useResult
  $Res call({StagedTimer stagedTimer});

  $StagedTimerCopyWith<$Res> get stagedTimer;
}

/// @nodoc
class _$StagedTimerEventCopyWithImpl<$Res, $Val extends StagedTimerEvent>
    implements $StagedTimerEventCopyWith<$Res> {
  _$StagedTimerEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stagedTimer = null,
  }) {
    return _then(_value.copyWith(
      stagedTimer: null == stagedTimer
          ? _value.stagedTimer
          : stagedTimer // ignore: cast_nullable_to_non_nullable
              as StagedTimer,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $StagedTimerCopyWith<$Res> get stagedTimer {
    return $StagedTimerCopyWith<$Res>(_value.stagedTimer, (value) {
      return _then(_value.copyWith(stagedTimer: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$StartedImplCopyWith<$Res>
    implements $StagedTimerEventCopyWith<$Res> {
  factory _$$StartedImplCopyWith(
          _$StartedImpl value, $Res Function(_$StartedImpl) then) =
      __$$StartedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({StagedTimer stagedTimer});

  @override
  $StagedTimerCopyWith<$Res> get stagedTimer;
}

/// @nodoc
class __$$StartedImplCopyWithImpl<$Res>
    extends _$StagedTimerEventCopyWithImpl<$Res, _$StartedImpl>
    implements _$$StartedImplCopyWith<$Res> {
  __$$StartedImplCopyWithImpl(
      _$StartedImpl _value, $Res Function(_$StartedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stagedTimer = null,
  }) {
    return _then(_$StartedImpl(
      stagedTimer: null == stagedTimer
          ? _value.stagedTimer
          : stagedTimer // ignore: cast_nullable_to_non_nullable
              as StagedTimer,
    ));
  }
}

/// @nodoc

class _$StartedImpl implements _Started {
  const _$StartedImpl({required this.stagedTimer});

  @override
  final StagedTimer stagedTimer;

  @override
  String toString() {
    return 'StagedTimerEvent.added(stagedTimer: $stagedTimer)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StartedImpl &&
            (identical(other.stagedTimer, stagedTimer) ||
                other.stagedTimer == stagedTimer));
  }

  @override
  int get hashCode => Object.hash(runtimeType, stagedTimer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$StartedImplCopyWith<_$StartedImpl> get copyWith =>
      __$$StartedImplCopyWithImpl<_$StartedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(StagedTimer stagedTimer) added,
    required TResult Function(StagedTimer stagedTimer) removed,
  }) {
    return added(stagedTimer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(StagedTimer stagedTimer)? added,
    TResult? Function(StagedTimer stagedTimer)? removed,
  }) {
    return added?.call(stagedTimer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(StagedTimer stagedTimer)? added,
    TResult Function(StagedTimer stagedTimer)? removed,
    required TResult orElse(),
  }) {
    if (added != null) {
      return added(stagedTimer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) added,
    required TResult Function(_Removed value) removed,
  }) {
    return added(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? added,
    TResult? Function(_Removed value)? removed,
  }) {
    return added?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? added,
    TResult Function(_Removed value)? removed,
    required TResult orElse(),
  }) {
    if (added != null) {
      return added(this);
    }
    return orElse();
  }
}

abstract class _Started implements StagedTimerEvent {
  const factory _Started({required final StagedTimer stagedTimer}) =
      _$StartedImpl;

  @override
  StagedTimer get stagedTimer;
  @override
  @JsonKey(ignore: true)
  _$$StartedImplCopyWith<_$StartedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RemovedImplCopyWith<$Res>
    implements $StagedTimerEventCopyWith<$Res> {
  factory _$$RemovedImplCopyWith(
          _$RemovedImpl value, $Res Function(_$RemovedImpl) then) =
      __$$RemovedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({StagedTimer stagedTimer});

  @override
  $StagedTimerCopyWith<$Res> get stagedTimer;
}

/// @nodoc
class __$$RemovedImplCopyWithImpl<$Res>
    extends _$StagedTimerEventCopyWithImpl<$Res, _$RemovedImpl>
    implements _$$RemovedImplCopyWith<$Res> {
  __$$RemovedImplCopyWithImpl(
      _$RemovedImpl _value, $Res Function(_$RemovedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stagedTimer = null,
  }) {
    return _then(_$RemovedImpl(
      stagedTimer: null == stagedTimer
          ? _value.stagedTimer
          : stagedTimer // ignore: cast_nullable_to_non_nullable
              as StagedTimer,
    ));
  }
}

/// @nodoc

class _$RemovedImpl implements _Removed {
  const _$RemovedImpl({required this.stagedTimer});

  @override
  final StagedTimer stagedTimer;

  @override
  String toString() {
    return 'StagedTimerEvent.removed(stagedTimer: $stagedTimer)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RemovedImpl &&
            (identical(other.stagedTimer, stagedTimer) ||
                other.stagedTimer == stagedTimer));
  }

  @override
  int get hashCode => Object.hash(runtimeType, stagedTimer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RemovedImplCopyWith<_$RemovedImpl> get copyWith =>
      __$$RemovedImplCopyWithImpl<_$RemovedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(StagedTimer stagedTimer) added,
    required TResult Function(StagedTimer stagedTimer) removed,
  }) {
    return removed(stagedTimer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(StagedTimer stagedTimer)? added,
    TResult? Function(StagedTimer stagedTimer)? removed,
  }) {
    return removed?.call(stagedTimer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(StagedTimer stagedTimer)? added,
    TResult Function(StagedTimer stagedTimer)? removed,
    required TResult orElse(),
  }) {
    if (removed != null) {
      return removed(stagedTimer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) added,
    required TResult Function(_Removed value) removed,
  }) {
    return removed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? added,
    TResult? Function(_Removed value)? removed,
  }) {
    return removed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? added,
    TResult Function(_Removed value)? removed,
    required TResult orElse(),
  }) {
    if (removed != null) {
      return removed(this);
    }
    return orElse();
  }
}

abstract class _Removed implements StagedTimerEvent {
  const factory _Removed({required final StagedTimer stagedTimer}) =
      _$RemovedImpl;

  @override
  StagedTimer get stagedTimer;
  @override
  @JsonKey(ignore: true)
  _$$RemovedImplCopyWith<_$RemovedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$StagedTimerState {
  KtList<StagedTimer> get stagedTimers => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $StagedTimerStateCopyWith<StagedTimerState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StagedTimerStateCopyWith<$Res> {
  factory $StagedTimerStateCopyWith(
          StagedTimerState value, $Res Function(StagedTimerState) then) =
      _$StagedTimerStateCopyWithImpl<$Res, StagedTimerState>;
  @useResult
  $Res call({KtList<StagedTimer> stagedTimers});
}

/// @nodoc
class _$StagedTimerStateCopyWithImpl<$Res, $Val extends StagedTimerState>
    implements $StagedTimerStateCopyWith<$Res> {
  _$StagedTimerStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stagedTimers = null,
  }) {
    return _then(_value.copyWith(
      stagedTimers: null == stagedTimers
          ? _value.stagedTimers
          : stagedTimers // ignore: cast_nullable_to_non_nullable
              as KtList<StagedTimer>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$StagedTimerStateImplCopyWith<$Res>
    implements $StagedTimerStateCopyWith<$Res> {
  factory _$$StagedTimerStateImplCopyWith(_$StagedTimerStateImpl value,
          $Res Function(_$StagedTimerStateImpl) then) =
      __$$StagedTimerStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({KtList<StagedTimer> stagedTimers});
}

/// @nodoc
class __$$StagedTimerStateImplCopyWithImpl<$Res>
    extends _$StagedTimerStateCopyWithImpl<$Res, _$StagedTimerStateImpl>
    implements _$$StagedTimerStateImplCopyWith<$Res> {
  __$$StagedTimerStateImplCopyWithImpl(_$StagedTimerStateImpl _value,
      $Res Function(_$StagedTimerStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? stagedTimers = null,
  }) {
    return _then(_$StagedTimerStateImpl(
      stagedTimers: null == stagedTimers
          ? _value.stagedTimers
          : stagedTimers // ignore: cast_nullable_to_non_nullable
              as KtList<StagedTimer>,
    ));
  }
}

/// @nodoc

class _$StagedTimerStateImpl implements _StagedTimerState {
  const _$StagedTimerStateImpl({required this.stagedTimers});

  @override
  final KtList<StagedTimer> stagedTimers;

  @override
  String toString() {
    return 'StagedTimerState(stagedTimers: $stagedTimers)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StagedTimerStateImpl &&
            (identical(other.stagedTimers, stagedTimers) ||
                other.stagedTimers == stagedTimers));
  }

  @override
  int get hashCode => Object.hash(runtimeType, stagedTimers);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$StagedTimerStateImplCopyWith<_$StagedTimerStateImpl> get copyWith =>
      __$$StagedTimerStateImplCopyWithImpl<_$StagedTimerStateImpl>(
          this, _$identity);
}

abstract class _StagedTimerState implements StagedTimerState {
  const factory _StagedTimerState(
          {required final KtList<StagedTimer> stagedTimers}) =
      _$StagedTimerStateImpl;

  @override
  KtList<StagedTimer> get stagedTimers;
  @override
  @JsonKey(ignore: true)
  _$$StagedTimerStateImplCopyWith<_$StagedTimerStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
