import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:staged_timer/domain/staged_timer/stage.dart';
import 'package:staged_timer/domain/staged_timer/staged_timer.dart';
import 'package:staged_timer/domain/staged_timer/value_objects.dart';

part 'staged_timer_event.dart';
part 'staged_timer_state.dart';
part 'staged_timer_bloc.freezed.dart';

@injectable
class StagedTimerBloc extends Bloc<StagedTimerEvent, StagedTimerState> {
  StagedTimerBloc() : super(StagedTimerState.initial()) {
    on<StagedTimerEvent>((event, emit) {
      event.map(
        added: (e) {
          final stagedTimers = state.stagedTimers.plusElement(e.stagedTimer);

          emit(state.copyWith(stagedTimers: stagedTimers));
        },
        removed: (e) {
          final stagedTimers = state.stagedTimers.minusElement(e.stagedTimer);

          emit(state.copyWith(stagedTimers: stagedTimers));
        },
      );
    });
  }
}
