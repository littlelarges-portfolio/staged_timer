part of 'staged_timer_bloc.dart';

@freezed
class StagedTimerEvent with _$StagedTimerEvent {
  const factory StagedTimerEvent.added({
    required StagedTimer stagedTimer,
  }) = _Started;
  const factory StagedTimerEvent.removed({
    required StagedTimer stagedTimer,
  }) = _Removed;
}
