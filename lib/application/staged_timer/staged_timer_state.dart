part of 'staged_timer_bloc.dart';

@freezed
class StagedTimerState with _$StagedTimerState {
  const factory StagedTimerState({
    required KtList<StagedTimer> stagedTimers,
  }) = _StagedTimerState;

  factory StagedTimerState.initial() => StagedTimerState(
        stagedTimers: KtList.from([
          StagedTimer(
            name: Name('Swimming'),
            stages: KtList.from([
              const Stage(time: Duration(seconds: 3)),
              const Stage(time: Duration(seconds: 3)),
              const Stage(time: Duration(seconds: 3)),
              const Stage(time: Duration(seconds: 3)),
            ]),
          ),
        ]),
      );
}
